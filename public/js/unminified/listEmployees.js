// Simple event listener, when u click on the button, u add a class : is-active
document.querySelector('#signin').addEventListener('click', () => {
    if(!document.querySelector('.is-active')) {
        document.querySelector('.modal').classList.add("is-active");
    }
});
// Simple event listener, when u click on the button, u remove a class : is-active
document.querySelector('#close-signin').addEventListener('click', () => {
    if(document.querySelector('.is-active')) {
        document.querySelector('.modal').classList.remove("is-active");
    }
});
// Ajax request ES6
function get(url) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('GET', url);
    req.onload = () => req.status === 200 ? resolve(req.response) : reject(Error(req.statusText));
    req.onerror = (e) => reject(Error(`Network Error: ${e}`));
    req.send();
  });
}
document.querySelector('#save-delete').addEventListener('click', () => {
    // Call AjaxController
    get('router.php')
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log('error');
    });
});