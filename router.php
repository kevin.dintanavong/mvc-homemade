<?php

require_once('controllers/IndexController.php');
require('models/workerTable.php');



if (isset($_GET['action']))
{
    $routing = new IndexController();
    if ($_GET['action'] == 'delete')
    {
        $routing->deleteWorkerAction($_GET['employee']);
    }
    elseif ($_GET['action'] == 'find')
    {
        $routing->findWorkerAction($_GET['employee']);
    }
    elseif ($_GET['updateWorkerAction'] == 'update')
    {
        $routing->updateWorkerAction($_GET['employee']);
    }
}
elseif (isset($_GET['create']))
{
    $routing->insertWorkerAction($_GET['employee']);
}
else
{
    $routing = new IndexController();
    $routing->renderAction();
}
