# mvc-homemade

Config :

A folder which contains some code to connect to the DB using PDO.

Controllers :

A folder which contains a list of Controllers

Models : 

A folder which contains a list of Models (SQL queries)

Public :

A folder which contains js, img, and css files

Vendor : 

All dependencies saved into composer.json

Node_modules :

All dependencies saved into package.json

.babelrc

Transpiler ES6

.gitignore

Ignore specific files / folders

gulpfile.js

A task runner



Use npm install

Node version > 7
In CLI, use gulp or gulp watch-js to execute gulp tasks

... and composer install

Add the DB to the project.
