<?php
// Class which contains a lot of method to render differents views and call differents actions
class IndexController
{
    public function renderAction()
    {
        // Create an object Twig (PHP template engine)
        $loader = new \Twig_Loader_Filesystem('views');
        $twig = new \Twig_Environment($loader);
        // Get a list of employees into models/workerTable.php
        $listEmployees = getWorker();
        // Get a list of column names
        $header = getNomColonneWorker();
        // Render view with twig, and pass variables into template views/list.html.twig
        echo $twig->render('list.html.twig', [
            'listEmployees' => $listEmployees,
            'header' => $header
        ]);
    }

    public function insertWorkerAction($value)
    {
        insertWorker($value['nom'], $value['prenom'], $value['mail'], $value['role'], $value['salaire']);
    }

    public function deleteWorkerAction($value)
    {
        deleteWorker($value);
        $this->renderAction();
    }

    public function findWorkerAction($value)
    {
        $employee = findWorker($value['id']);
        // Create an object Twig (PHP template engine)
        $loader = new \Twig_Loader_Filesystem('views');
        $twig = new \Twig_Environment($loader);
        $header = getNomColonneWorker();
        // Render view with twig, and pass variables into template views/list.html.twig
        echo $twig->render('list.html.twig', [
            'listEmployees' => $employee,
            'header' => $header
        ]);
    }

    public function updateWorkerAction(HttpRequest $value)
    {
        updateWorker($value['id'], $value['nom'], $value['prenom'], $value['mail'], $value['role'], $value['salaire']);
        $this->renderAction();
    }
}
