// Call modules into node_modules
// Call Gulp, a task runner
const gulp = require('gulp');
// Call SASS for Gulp
const sass = require('gulp-sass');
// Call autoprefixer (compatibility browser) for Gulp
const autoprefixer = require('gulp-autoprefixer');
// Call CSSO to minify CSS
const csso = require('gulp-csso');
// Call uglify to minify JS
const uglify = require('gulp-uglify');
// Call babel, a transpiler which transforms ES6 and more in older code
const babel = require('gulp-babel');
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];
// Call a task of Gulp called 'sass', which check all scss files, minify and transform in CSS
gulp.task('sass',() => {
  return gulp.src('public/scss/*.scss')
  .pipe(sass())
	.pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
  .pipe(csso())
  .pipe(gulp.dest('public/css'))
});
// Call a task of Gulp called 'scripts', which check all js files, minify and transform in older JS
gulp.task('scripts', () => {
  return gulp.src('public/js/unminified/*.js')
  .pipe(babel({presets: ['es2015']}))
  .pipe(uglify())
  .pipe(gulp.dest('public/js/minified'))
});
// Call sass task when you write 'gulp', gulp is watching in real time all scss files
gulp.task('default', () => {
	gulp.watch('public/scss/*.scss', ['sass']);
});
// Call scripts task when you write 'gulp watch-js', gulp is watching in real time all js files
gulp.task('watch-js', () => {
  gulp.watch('public/js/unminified/*.js', ['scripts']);
});