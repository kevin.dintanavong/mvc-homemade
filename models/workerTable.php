<?php

// Get the columns names of the table worker
// return the result in a array
function getNomColonneWorker()
{
    $stmt = $_SESSION['dbh']->prepare("SELECT `COLUMN_NAME`
            FROM `INFORMATION_SCHEMA`.`COLUMNS`
            WHERE TABLE_NAME = :table");

    $stmt->bindValue(':table', "worker");

    if ($stmt->execute()) {
        return $stmt->fetchAll();
    }

    return NULL;
}

// Get all the rows of worker
// return the result in a array
function getWorker()
{
    $stmt = $_SESSION['dbh']->prepare("SELECT * FROM worker");

    if ($stmt->execute()) {
        return $stmt->fetchAll();
    }

    return NULL;
}

// Insert a new row in the table worker
function insertWorker($_nom, $_prenom, $_mail, $_role, $_salaire)
{
    $stmt = $_SESSION['dbh']->prepare("INSERT INTO worker (nom, prenom, mail, role, salaire) VALUES (:nom, :prenom, :mail, :role, :salaire)");
    $stmt->bindParam(':nom', $_nom);
    $stmt->bindParam(':prenom', $_prenom);
    $stmt->bindParam(':mail', $_mail);
    $stmt->bindParam(':role', $_role);
    $stmt->bindParam(':salaire', $_salaire);

    $stmt->execute();
}

// Delete one or some rows of the table worker
// Array ids with the differents ids of the row(s) we want to delete
function deleteWorker($ids)
{
    $stmt = $_SESSION['dbh']->prepare("DELETE FROM worker WHERE id = :id");

    foreach ($ids as  $value)
    {
        $stmt->bindParam(':id', $value);
        $stmt->execute();
    }
}

// Find a row in the table worker
// return the result in a array
function findWorker($id)
{
    $stmt = $_SESSION['dbh']->prepare("SELECT * FROM worker WHERE id = :id");
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        return $stmt->fetch();
    }

    return NULL;
}

// Update a row in the table worker
function updateWorker($_id, $_nom, $_prenom, $_mail, $_role, $_salaire)
{
    $stmt = $_SESSION['dbh']->prepare("UPDATE worker SET nom = :nom, prenom = :prenom, mail = :mail, role = :role, salaire = :salaire WHERE id = :id");
    $stmt->bindParam(':id', $_id);
    $stmt->bindParam(':nom', $_nom);
    $stmt->bindParam(':prenom', $_prenom);
    $stmt->bindParam(':mail', $_mail);
    $stmt->bindParam(':role', $_role);
    $stmt->bindParam(':salaire', $_salaire);

    $stmt->execute();
}
